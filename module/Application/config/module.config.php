<?php

return array(
    /**
     * Database options.
     */
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=DB_NAME_HERE;host=HOST_NAME_HERE',
        'driver_options' => array(
            // default database charset to utf8
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'utf8\''
        ),
        // IMPORTANT! the following options should be set on /config/autoload/local.php
        // to avoid repository propagation.
        'username' => 'USERNAME_HERE',
        'password' => 'PASSWORD_HERE',
        'enable_profiler' => false,
    ),
    /**
     * Router configuration.
     */
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    /**
     * Session manager
     */
    'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'application',
                'remember_me_seconds' => 86400,
            ),
        ),
        // Uncomment 'save_handler' to define another way to save session data;
        // DbTableGateway stores sessions on database and is indicated for balanced apps.
        //'save_handler' => 'Zend\Session\SaveHandler\DbTableGateway',
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            array(
                'Zend\Session\Validator\RemoteAddr',
                'Zend\Session\Validator\HttpUserAgent',
            ),
        ),
    ),
    /**
     * Service manager configuration.
     */
    'service_manager' => array(
        'invokables' => array(
            'Application\Service\Application' => 'Application\Service\ApplicationService',
        ),
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Application\Db\Adapter\AdapterServiceFactory',
            'Zend\Session\SessionManager' => 'Application\Session\SessionManagerFactory',
            'Zend\Session\SaveHandler\DbTableGateway' => 'Application\Session\SaveHandler\DbTableGatewayFactory',
        ),
    ),
    /**
     * Controllers configuration.
     */
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
        ),
    ),
    /**
     * View manager configuration.
     */
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
