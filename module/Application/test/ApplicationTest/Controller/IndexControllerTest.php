<?php

namespace ApplicationTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                include realpath(__DIR__ . '/../../../../../config/application.config.php')
        );

        $applicationService = $this->getMockBuilder('Application\Service\ApplicationService')
                ->disableOriginalConstructor()
                ->getMock();

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Application\Service\Application', $applicationService);
        
        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('Application');
        $this->assertControllerName('Application\Controller\Index');
        $this->assertControllerClass('IndexController');
        $this->assertMatchedRouteName('home');
    }

    public function notestIndexViewHasTitle()
    {
        $this->dispatch('/');
        $this->assertQuery('head title');
        $this->assertQueryContentContains('head title', 'Maido');
    }

}
