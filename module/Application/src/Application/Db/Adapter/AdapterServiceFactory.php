<?php

namespace Application\Db\Adapter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Adapter\Adapter;
use BjyProfiler\Db\Adapter\ProfilingAdapter;
use BjyProfiler\Db\Profiler\Profiler;

class AdapterServiceFactory implements FactoryInterface
{

    /**
     * Create db adapter service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Adapter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        if (isset($config['db']['enable_profiler']) && true === $config['db']['enable_profiler']) {
            $adapter = new ProfilingAdapter($config['db']);
            $adapter->setProfiler(new Profiler);

            if (isset($dbParams['options']) && is_array($dbParams['options'])) {
                $options = $dbParams['options'];
            } else {
                $options = array();
            }

            $adapter->injectProfilingStatementPrototype($options);
        } else {
            $adapter = new Adapter($config['db']);
        }
        return $adapter;
    }

}
